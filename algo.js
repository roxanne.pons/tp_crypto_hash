//usefull stuff:
//https://nodejs.org/api/readline.html
//https://gist.github.com/initlove/2478016


// algorithme de hashing de Tim
const calculateHash = str => {
  let hash = str
      .split("")
      .map((c, i) => str.charCodeAt(i)).map(c => c + 2)
      .map(c => String.fromCharCode(c)).join("");
  return Buffer.from(hash).toString("base64");
};

//------------------------------------------------------------
//MODULES DONT ON A BESOIN:
// module pour interagir avec le file system
const fs = require('fs');
//module qui permet de lire les fichiers ligne par ligne
const readline = require('readline');

//ACCES + LECTURE DU FICHIER .TXT
//interface pour lire le fichier
const rl = readline.createInterface({
  //ouvre notre file et lis les data dedans
  input: fs.createReadStream('possibilities.txt'),
});

//PASSAGE EN REVUE DE LA LISTE (système de comparaison)
//event 'line' appelé sur chaque ligne de la liste
//listener 'pswd' stock la seule ligne correspondante
rl.on('line', (pswd) => {
  //on appelle la fonction de Tim sur le pswd
  const word = calculateHash(pswd);
  //on regarde si le listener pwsd correspond au hash
  if (word == 'ZWpxZQ==') {
      //si oui, affichage
      console.log("hash: ", word);
      console.log("pswd: ", pswd);
  }
})

//fermer linterface et le stream de lecture
rl.on('close', () => {
  return process.exit(1);
})


//run script with 'node algo.js'